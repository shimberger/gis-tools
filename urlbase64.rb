#!/usr/bin/env ruby

require 'base64' unless defined?(Base64)

STDOUT.print Base64.encode64(STDIN.read).tr('+/', '-_')
