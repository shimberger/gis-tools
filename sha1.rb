#!/usr/bin/env ruby

require 'digest/sha1'

STDOUT.print Digest::SHA1.digest(STDIN.read)

