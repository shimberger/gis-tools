==== GIS Tools ===

Just some simple ruby scripts for GIS scripting.

==== Dependencies ====

Uses the GEOS FFI so you need GEOS and the Gem from https://github.com/dark-panda/ffi-geos/.

Install the Gem using gem install ffi-geos

==== Examples ====

  echo "POINT(1 1)" | ./wkt2wkb.rb > point_1_1.wkb

==== Useful Links ====

 * http://geos.osgeo.org/doxygen/classgeos_1_1io_1_1WKBWriter.html
 * https://github.com/zoocasa/geos-extensions/blob/master/lib/geos_extensions.rb

