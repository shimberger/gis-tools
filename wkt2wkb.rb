#!/usr/bin/env ruby

require 'ffi-geos' unless defined?(Geos)

wkt_reader = Geos::WktReader.new
wkb_writer = Geos::WkbWriter.new

wkt  = wkt_reader.read(STDIN.read)
STDOUT.print wkb_writer.write(wkt)

